﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chat_app.Entities
{
    public class Problem
    {
        public int ID { get; set; }
        public Room Room { get; set; } 


        public string Title { get; set; }
        public string Task { get; set;  }
        public string Input { get; set; } = "";
        public string Output { get; set; } = "";

        public string Source { get; set; } = "";

        public string Language {get;set;} = ""; 

        public object ToObject()
        {
            return new
            {
                ID = ID,
                RoomID = Room.ID,
                Title = Title,
                Task  = Task,
                Input = Input,
                Output = Output,
                Source = Source,
                Lang = Language
            };
        }


    }
}
