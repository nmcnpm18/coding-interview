﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chat_app.Entities
{
    public class Room
    {
        public int ID { get; set; }
        public User MasterUser { get; set; }

        public string IntervieweeName { get; set; }
        public string Name { get; set;  }

        public string LoginString { get; set; }
        public string Password { get; set; }

        public int CurrentRating { get; set; }

        public object ToObject()
        {
            return new
            {
                roomID = ID,
                roomMasterId = MasterUser.ID,
                roomMasterName = MasterUser.FirstName + " " +  MasterUser.LastName,
                roomLoginString = LoginString,
                roomPassword = Password,
                roomRating = CurrentRating,


            };
        }
        

    }
    
}
