﻿import axios from 'axios';

let API_URL = 'https://localhost:44326/api';

export default function callApi(endpoint, method = 'GET', body) {
    return axios({
        method,
        url: `${API_URL}/${endpoint}`,
        data: body,
    }).catch((e) => {
        console.log(e);
    });
}
