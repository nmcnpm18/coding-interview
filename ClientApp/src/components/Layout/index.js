import React, { Component } from 'react';
import { NavBar } from '../NavBar';
import { Container } from 'react-bootstrap';

export class Layout extends Component {
    render() {
        return (
            <Container fluid>
                <NavBar />
                <div>{this.props.children}</div>
            </Container>
        );
    }
}
