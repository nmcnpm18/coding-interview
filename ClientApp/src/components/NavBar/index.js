import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Navbar, Nav, Image } from 'react-bootstrap';
import { userService } from '../../services';

import './navBar.css';

export function NavBar(props) {
    const history = useHistory();
    const [key, setKey] = useState('home');

    const handleSelect = (eventKey) => {
        setKey(eventKey);
    };

    const handleLogout = () => {
        userService.logout();
        history.push('/login');
    };

    const user = localStorage.getItem('user');
    const userInfo = user && JSON.parse(user);

    return user ? (
        <Navbar collapseOnSelect expand='sm' bg='dark' variant='dark'>
            <Navbar.Brand>
                <Image src='logo.jpeg' className='logo' />
                <Nav.Link
                    eventKey='1'
                    className='navbar-brand'
                    as={Link}
                    to='/'
                    onSelect={() => {
                        handleSelect('home');
                    }}
                >
                    Coding-Interview
                </Nav.Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls='responsive-navbar-nav' />
            <Navbar.Collapse id='responsive-navbar-nav'>
                <Nav
                    className='ml-auto'
                    activeKey={key}
                    onSelect={handleSelect}
                >
                    <Nav.Item>
                        <Nav.Link eventKey='home' as={Link} to='/'>
                            Home
                        </Nav.Link>
                    </Nav.Item>

                    <Nav.Item>
                        <Nav.Link eventKey='about' as={Link} to='/about'>
                            About
                        </Nav.Link>
                    </Nav.Item>

                    <Nav.Item>
                        <Nav.Link eventKey='profile' as={Link} to='/profile'>
                            {userInfo.name}
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey='logout' onClick={handleLogout}>
                            Logout
                        </Nav.Link>
                    </Nav.Item>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    ) : (
        <Navbar collapseOnSelect expand='sm' bg='dark' variant='dark'>
            <Navbar.Brand>
                <Image src='logo.jpeg' className='logo' />
                <Nav.Link
                    eventKey='1'
                    className='navbar-brand'
                    as={Link}
                    to='/'
                    onSelect={() => {
                        handleSelect('home');
                    }}
                >
                    Coding-Interview
                </Nav.Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls='responsive-navbar-nav' />
            <Navbar.Collapse id='responsive-navbar-nav'>
                <Nav
                    className='ml-auto'
                    activeKey={key}
                    onSelect={handleSelect}
                >
                    <Nav.Item>
                        <Nav.Link eventKey='home' as={Link} to='/'>
                            Home
                        </Nav.Link>
                    </Nav.Item>

                    <Nav.Item>
                        <Nav.Link eventKey='about' as={Link} to='/about'>
                            About
                        </Nav.Link>
                    </Nav.Item>

                    <Nav.Item>
                        <Nav.Link eventKey='login' as={Link} to='/login'>
                            Login
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey='register' as={Link} to='/register'>
                            Register
                        </Nav.Link>
                    </Nav.Item>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
