import React from 'react';

export function About() {
    return (
        <div>
            <h1>Our team</h1>
            <p>Võ Xuân Hoà - Project Manager</p>
            <p>Võ Thiện An - Backend Developer</p>
            <p>Lê Văn Trung - Frontend Developer</p>
            <p>Nguyễn Cao Đăng Khoa - Tester</p>
        </div>
    );
}
