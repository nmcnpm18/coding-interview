import React, { Component, useEffect, useRef } from 'react';
import './roomPage.css';
import { Tabs, Tab, Col, Row, Nav } from 'react-bootstrap';
import { UnControlled as CodeMirror } from 'react-codemirror2';
import axios from 'axios';
import * as SignalR from '@aspnet/signalr';
import TelegramIcon from '@material-ui/icons/Telegram';
import { authHeader } from '../../../helpers';
require('codemirror/lib/codemirror.css');
require('codemirror/theme/material.css');
require('codemirror/mode/clike/clike');
require('codemirror/mode/php/php');
require('codemirror/mode/python/python');
require('codemirror/mode/swift/swift');
require('codemirror/mode/javascript/javascript');
require('codemirror/mode/go/go');
function Message(props) {
    let messagesEnd = useRef(null);
    const scrollToBottom = () => {
        messagesEnd.scrollIntoView({ behavior: 'smooth' });
    };

    useEffect(() => {
        scrollToBottom();
    });

    return (
        <div>
            <div className='message-container'>
                {props.data.map((child) => {
                    return (
                        <div className='message'>
                            <span className='user-name'>{child.user}</span>
                            <span>:&nbsp;&nbsp;</span>
                            <span>{child.text}</span>
                        </div>
                    );
                })}
            </div>
            <div
                style={{ float: 'left', clear: 'both' }}
                ref={(el) => {
                    messagesEnd = el;
                }}
            ></div>
        </div>
    );
}
class RoomPage extends Component {
    constructor(props) {
        super(props);
        const roomData = this.props.history.location.state;

        this.state = {
            problems: [],
            problem: {
                eventKey: '',
                title: '',
                task: '',
                input: '',
                output: '',
                source: '',
                lang: 'text/x-csharp',
                isSuccessSubmit: false,
            },
            label: 'problems',
            options: {
                mode: 'text/x-csharp',
                theme: 'material',
                lineNumbers: true,
            },
            value: '//Your code goes here...',
            // #include<iostream>\nusing namespace std;\nint main(){\ncout<<"Hello world!"<<endl;\nreturn 0;\n}
            message: { user: '', text: '' },
            messages: [],
            hubConnection: null,
            hasAccess: false,
            addProblemErr: false,
            candidateName: '',
            userMess: '',
            isLoadingOutput: false,
            addProblemError: false,
            ...roomData,
        };
    }

    componentDidMount() {
        const hubConnection = new SignalR.HubConnectionBuilder()
            .withUrl('/chatHub')
            .configureLogging(SignalR.LogLevel.Information)
            .build();

        this.setState({ hubConnection: hubConnection }, () => {
            this.state.hubConnection
                .start()
                .then(() => {
                    console.log('Connection started!');
                    this.state.hubConnection
                        .invoke('JoinRoom', +this.state.roomID)
                        .catch((err) => console.error(err));

                    this.state.hubConnection.on(
                        'ReceiveMessage',
                        (user, message) => {
                            this.setState({
                                messages: [
                                    ...this.state.messages,
                                    { user, text: message },
                                ],
                            });
                        }
                    );
                })
                .catch((err) =>
                    console.log('Error while establishing connection')
                );
        });

        const requestBody = {
            roomID: this.state.roomID.toString(),
        };

        console.log(requestBody);

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        };

        fetch(
            `${process.env.REACT_APP_API}/room/getRoomProblems`,
            requestOptions
        ).then((response) => {
            return response.text().then((text) => {
                const data = text && JSON.parse(text);

                if (response.ok) {
                    data.map((child, index) => {
                        child.eventKey = `problem${index + 1}`;
                        return child;
                    });
                    console.log(data);
                    this.setState({ problems: data });
                }
            });
        });
    }

    handleSolve = (e) => {
        const index = e.target.getAttribute('data-index');
        this.setState({ label: this.state.problems[index].eventKey });
    };

    setKey = (k) => {
        let mode = '';

        for (const child of this.state.problems) {
            if (child.eventKey == k) {
                mode = child.lang;
            }
        }

        this.setState({
            label: k,
            options: { ...this.state.options, mode: mode },
        });
    };

    handleCodeChange = (editor, data, value) => {
        const newProblems = this.state.problems.map((child) => {
            if (child.eventKey == this.state.label) {
                child.source = value;
            }
            return child;
        });
    };

    handleLanguage = (e) => {
        const newProblems = this.state.problems.map((child) => {
            if (child.eventKey == this.state.label) {
                child.lang = e.target.value;
            }
            return child;
        });

        this.setState({
            problems: newProblems,
            options: { ...this.state.options, mode: e.target.value },
        });
    };

    handleSubmit = () => {
        let data = {};
        for (let child of this.state.problems) {
            if (child.eventKey == this.state.label) {
                data = child;
            }
        }

        const requestBody = {
            ID: `${data.id}`,
            Input: `${data.input}`,
            Output: `${data.output}`,
            Source: `${data.source}`,
            Lang: `${data.lang}`,
        };

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        };

        fetch(
            `${process.env.REACT_APP_API}/room/updateproblem`,
            requestOptions
        ).then((response) => {
            return response.text().then((text) => {
                const data = text && JSON.parse(text);

                if (response.ok) {
                    const newProblems = this.state.problems.map((child) => {
                        if (child.eventKey == this.state.label) {
                            child.isSuccessSubmit = true;
                        }
                        return child;
                    });
                    console.log(newProblems);
                    this.setState({ problems: newProblems });
                }
            });
        });
    };

    handleGetSubmission = () => {
        const requestBody = {
            roomID: this.state.roomID.toString(),
        };

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        };

        fetch(
            `${process.env.REACT_APP_API}/room/getRoomProblems`,
            requestOptions
        ).then((response) => {
            return response.text().then((text) => {
                const data = text && JSON.parse(text);

                if (response.ok) {
                    const newProblems = data.map((child, index) => {
                        child.eventKey = `problem${index + 1}`;
                        return child;
                    });
                    console.log(data);

                    this.setState({ problems: newProblems });
                }
            });
        });
    };

    handleProblemTitleChange = (e) => {
        this.setState({
            problem: { ...this.state.problem, title: e.target.value },
        });
    };

    handleProblemTaskChange = (e) => {
        this.setState({
            problem: { ...this.state.problem, task: e.target.value },
        });
    };

    handleProblemInputChange = (e) => {
        this.setState({
            problem: { ...this.state.problem, input: e.target.value },
        });
    };

    handleProblemLanguageChange = (e) => {
        this.setState({
            problem: { ...this.state.problem, lang: e.target.value },
        });
    };

    handleAddProblem = async (e) => {
        e.preventDefault();

        if (
            this.state.problem.title.trim().length == 0 ||
            this.state.problem.task.trim().length == 0 ||
            this.state.problem.input.trim().length == 0 ||
            this.state.problem.lang.trim().length == 0
        ) {
            this.setState({
                addProblemError: true,
            });

            return;
        }
        const eventKey = `problem${this.state.problems.length + 1}`;
        const problem = {
            ...this.state.problem,
            eventKey,
        };

        const newProblems = [...this.state.problems, problem];

        const requestBody = {
            roomID: this.state.roomID.toString(),
            Title: problem.title,
            Task: problem.task,
            Input: problem.input,
            Source: problem.source,
            Lang: problem.lang,
        };

        console.log(requestBody);

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                ...authHeader(),
            },
            body: JSON.stringify(requestBody),
        };

        await fetch(
            `${process.env.REACT_APP_API}/room/createProblem`,
            requestOptions
        ).then((response) => {
            return response.text().then((text) => {
                const data = text && JSON.parse(text);

                console.log(data);

                if (response.ok) {
                    this.setState({
                        problems: newProblems,
                        addProblemErr: false,
                        addProblemError: false,
                    });
                } else {
                    this.setState({
                        addProblemErr: true,
                        addProblemError: true,
                    });
                }
            });
        });
    };

    handleTest = async () => {
        const lang = {
            'text/x-csrc': { lang: 'c', sample: '' },
            'text/x-c++src': { lang: 'cpp', sample: '' },
            'text/x-csharp': { lang: 'csharp', sample: '' },
            'text/javascript': { lang: 'nodejs', sample: '' },
            'text/x-java': { lang: 'java', sample: '' },
            'text/x-php': { lang: 'php', sample: '' },
            'text/x-python': { lang: 'python3', sample: '' },
            'text/x-swift': { lang: 'swift', sample: '' },
            'text/x-go': { lang: 'go', sample: '' },
        };

        let result = {
            clientId: 'c3b325fc897b24d9b54d833c36c2acf4',
            clientSecret:
                '9771e1ce2219a76f3807d9946ac00c766899d36aaff0c5149fd1d3b3620a654d',
            script: '',
            stdin: '',
            language: '',
            versionIndex: '3',
        };

        for (const child of this.state.problems) {
            if (child.eventKey == this.state.label) {
                result.script = child.source;
                result.stdin = child.input;
                result.language = lang[child.lang].lang;
            }
        }

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(result),
        };

        this.setState({ isLoadingOutput: true });

        await fetch(
            `https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute/`,
            requestOptions
        ).then((res) => {
            return res.text().then((text) => {
                const data = text && JSON.parse(text);

                console.log(data);

                const newProblems = this.state.problems.map((child) => {
                    if (child.eventKey == this.state.label) {
                        child.output = data.output;
                    }
                    return child;
                });
                this.setState({
                    problems: newProblems,
                    isLoadingOutput: false,
                });
            });
        });
    };

    handleReloadProblems = () => {
        const requestBody = {
            roomID: this.state.roomID.toString(),
        };

        console.log(requestBody);

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        };

        fetch(
            `${process.env.REACT_APP_API}/room/getRoomProblems`,
            requestOptions
        ).then((response) => {
            return response.text().then((text) => {
                const data = text && JSON.parse(text);

                if (response.ok) {
                    data.map((child, index) => {
                        child.eventKey = `problem${index + 1}`;
                        return child;
                    });
                    console.log(data);
                    this.setState({ problems: data });
                }
            });
        });
    };

    handleInput = (e) => {
        const newProblems = this.state.problems.map((child) => {
            if (child.eventKey == this.state.label) {
                child.input = e.target.value;
            }
            return child;
        });

        this.setState({ problems: newProblems });
    };

    appendMessage = () => {
        if (this.state.userMess.trim().length == 0) {
            return;
        }

        const user = this.state.isMaster
            ? this.state.roomMasterName
            : this.state.candidateName;

        this.state.hubConnection.invoke(
            'SendMessegeToRoom',
            +this.state.roomID,
            user,
            this.state.userMess
        );

        this.setState({
            userMess: '',
        });
    };

    setUserMess = (e) => {
        this.setState({ userMess: e.target.value });
    };

    handleLeaveButton = () => {
        if (this.state.isMaster) {
            return this.props.history.push('/rating', {
                roomID: this.state.roomID,
            });
        }
        this.props.history.push('/');
    };

    render() {
        if (this.state.hasAccess) {
        } else {
            this.props.history.push('/');
        }

        let content = <div></div>;
        if (this.state.isMaster) {
            content = (
                <>
                    <div className='code-area'>
                        {
                            <Tabs
                                variant='tabs'
                                activeKey={this.state.label}
                                onSelect={this.setKey}
                            >
                                <Tab
                                    className='problem-container'
                                    eventKey='problems'
                                    title='Problems'
                                >
                                    <div className='problem-area'>
                                        <div>
                                            <h3 className='add-problem-title'>
                                                Add problems
                                            </h3>
                                            <form>
                                                <div>
                                                    <p>
                                                        {this.state
                                                            .addProblemError
                                                            ? 'Please fill all the blank.'
                                                            : null}
                                                    </p>
                                                    <p>
                                                        {this.state
                                                            .addProblemErr
                                                            ? 'Some errors occured.'
                                                            : null}
                                                    </p>
                                                </div>
                                                <div>
                                                    <label className='problem-label'>
                                                        Problem title
                                                    </label>
                                                    <textarea
                                                        className='problem-input problem-title-input'
                                                        onChange={
                                                            this
                                                                .handleProblemTitleChange
                                                        }
                                                    />
                                                </div>
                                                <div>
                                                    <label className='problem-label'>
                                                        Problem description
                                                    </label>
                                                    <textarea
                                                        className='problem-input problem-desc-input'
                                                        onChange={
                                                            this
                                                                .handleProblemTaskChange
                                                        }
                                                    />
                                                </div>
                                                <div>
                                                    <label className='problem-label'>
                                                        Input
                                                    </label>
                                                    <textarea
                                                        className='problem-input problem-input-input'
                                                        onChange={
                                                            this
                                                                .handleProblemInputChange
                                                        }
                                                    />
                                                </div>
                                                <div>
                                                    <label className='problem-label'>
                                                        Language
                                                    </label>
                                                    <select
                                                        className='problem-lang-input'
                                                        onChange={
                                                            this
                                                                .handleProblemLanguageChange
                                                        }
                                                    >
                                                        <option value='text/x-csrc'>
                                                            C
                                                        </option>
                                                        <option value='text/x-c++src'>
                                                            C++
                                                        </option>
                                                        <option
                                                            selected
                                                            value='text/x-csharp'
                                                        >
                                                            C#
                                                        </option>
                                                        <option value='text/javascript'>
                                                            Javascript
                                                        </option>
                                                        <option value='text/x-java'>
                                                            Java
                                                        </option>
                                                        <option value='text/x-php'>
                                                            PHP
                                                        </option>
                                                        <option value='text/x-python'>
                                                            Python
                                                        </option>
                                                        <option value='text/x-swift'>
                                                            Swift
                                                        </option>
                                                        <option value='text/x-go'>
                                                            Go
                                                        </option>
                                                        <option></option>
                                                    </select>
                                                </div>
                                                <div className='add-problem-btn-container'>
                                                    <button
                                                        className='btn btn-primary  add-problem-btn'
                                                        onClick={
                                                            this
                                                                .handleAddProblem
                                                        }
                                                    >
                                                        Add Problem
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </Tab>
                                {this.state.problems.map((child, index) => {
                                    return (
                                        <Tab
                                            className='problem-container'
                                            key={index}
                                            eventKey={child.eventKey}
                                            title={child.title}
                                        >
                                            <div className='code-language'>
                                                <span className='mr-1'>
                                                    Select your language
                                                </span>
                                                <select
                                                    value={child.lang}
                                                    onChange={
                                                        this.handleLanguage
                                                    }
                                                >
                                                    <option value='text/x-csrc'>
                                                        C
                                                    </option>
                                                    <option value='text/x-c++src'>
                                                        C++
                                                    </option>
                                                    <option
                                                        selected
                                                        value='text/x-csharp'
                                                    >
                                                        C#
                                                    </option>
                                                    <option value='text/javascript'>
                                                        Javascript
                                                    </option>
                                                    <option value='text/x-java'>
                                                        Java
                                                    </option>
                                                    <option value='text/x-php'>
                                                        PHP
                                                    </option>
                                                    <option value='text/x-python'>
                                                        Python
                                                    </option>
                                                    <option value='text/x-swift'>
                                                        Swift
                                                    </option>
                                                    <option value='text/x-go'>
                                                        Go
                                                    </option>
                                                    <option></option>
                                                </select>
                                            </div>
                                            <CodeMirror
                                                value={
                                                    child.source.length == 0
                                                        ? this.state.value
                                                        : child.source
                                                }
                                                options={this.state.options}
                                                onChange={this.handleCodeChange}
                                            />
                                            <div className='row code-nav-area'>
                                                <div className='input-area col-5'>
                                                    <p>Input</p>
                                                    <textarea
                                                        value={child.input}
                                                        className='form-control'
                                                        onChange={
                                                            this.handleInput
                                                        }
                                                    ></textarea>
                                                </div>
                                                <div className='output-area col-5'>
                                                    <p>Output</p>
                                                    <textarea
                                                        value={
                                                            this.state
                                                                .isLoadingOutput
                                                                ? 'Loading...'
                                                                : child.output
                                                        }
                                                        className='form-control'
                                                    ></textarea>
                                                </div>
                                                <div className='col-2  code-nav-btn'>
                                                    <button
                                                        className='btn btn-success mb-3'
                                                        onClick={
                                                            this.handleTest
                                                        }
                                                    >
                                                        Test
                                                    </button>
                                                    <button
                                                        className='btn btn-success get-submission-btn'
                                                        onClick={
                                                            this
                                                                .handleGetSubmission
                                                        }
                                                    >
                                                        Get submission
                                                    </button>
                                                </div>
                                            </div>
                                        </Tab>
                                    );
                                })}
                            </Tabs>
                        }
                    </div>
                    <div className='nav-button'>
                        <button className='btn nav-btn'>
                            <i
                                className='fa fa-microphone'
                                aria-hidden='true'
                            ></i>
                        </button>
                        <button className='btn nav-btn'>
                            <i className='fa fa-shield' aria-hidden='true'></i>
                        </button>
                        <button className='btn nav-btn'>
                            <i className='fas fa-comment-dots'></i>
                        </button>
                        <button className='btn nav-btn'>
                            <i className='fas fa-info-circle'></i>
                        </button>
                        <button
                            className='btn leave-btn'
                            onClick={() => {
                                if (
                                    window.confirm(
                                        'Are you sure to leave the interview?'
                                    )
                                )
                                    this.handleLeaveButton();
                            }}
                        >
                            Leave
                        </button>
                    </div>
                </>
            );
        } else {
            content = (
                <>
                    <div className='code-area'>
                        {this.state.problems.length == 0 ? (
                            <Tabs variant='tabs'>
                                <Tab eventKey='problems' title='Problems'>
                                    <button
                                        className='reload-problem-btn'
                                        onClick={this.handleReloadProblems}
                                    >
                                        Reload Problems
                                    </button>
                                </Tab>
                            </Tabs>
                        ) : (
                            <Tabs
                                variant='tabs'
                                activeKey={this.state.label}
                                onSelect={this.setKey}
                            >
                                <Tab
                                    className='problem-container'
                                    eventKey='problems'
                                    title='Problems'
                                >
                                    <div className='problem-area'>
                                        {this.state.problems.map(
                                            (child, index) => {
                                                return (
                                                    <div>
                                                        <div>
                                                            <h1>
                                                                {child.title}
                                                            </h1>
                                                            <p>{child.task}</p>
                                                            <p>{child.input}</p>
                                                            <p>
                                                                {child.output}
                                                            </p>
                                                        </div>
                                                        <div className='problem-btn'>
                                                            <button
                                                                className='btn btn-success'
                                                                data-index={
                                                                    index
                                                                }
                                                                onClick={
                                                                    this
                                                                        .handleSolve
                                                                }
                                                            >
                                                                Solve
                                                            </button>
                                                        </div>
                                                    </div>
                                                );
                                            }
                                        )}
                                    </div>
                                </Tab>
                                {this.state.problems.map((child, index) => {
                                    return (
                                        <Tab
                                            className='problem-container'
                                            key={index}
                                            eventKey={child.eventKey}
                                            title={child.title}
                                        >
                                            <div className='code-language'>
                                                <span className='mr-1'>
                                                    Select your language
                                                </span>
                                                <select
                                                    onChange={
                                                        this.handleLanguage
                                                    }
                                                >
                                                    <option value='text/x-csrc'>
                                                        C
                                                    </option>
                                                    <option value='text/x-c++src'>
                                                        C++
                                                    </option>
                                                    <option
                                                        selected
                                                        value='text/x-csharp'
                                                    >
                                                        C#
                                                    </option>
                                                    <option value='text/javascript'>
                                                        Javascript
                                                    </option>
                                                    <option value='text/x-java'>
                                                        Java
                                                    </option>
                                                    <option value='text/x-php'>
                                                        PHP
                                                    </option>
                                                    <option value='text/x-python'>
                                                        Python
                                                    </option>
                                                    <option value='text/x-swift'>
                                                        Swift
                                                    </option>
                                                    <option value='text/x-go'>
                                                        Go
                                                    </option>
                                                    <option></option>
                                                </select>
                                            </div>
                                            <CodeMirror
                                                value={
                                                    child.source.length == 0
                                                        ? this.state.value
                                                        : child.source
                                                }
                                                options={this.state.options}
                                                onChange={this.handleCodeChange}
                                            />
                                            <div className='row code-nav-area'>
                                                <div className='input-area col-5'>
                                                    <p>Input</p>
                                                    <textarea
                                                        value={child.input}
                                                        className='form-control'
                                                        onChange={
                                                            this.handleInput
                                                        }
                                                    ></textarea>
                                                </div>
                                                <div className='output-area col-5'>
                                                    <p>Output</p>
                                                    <textarea
                                                        value={
                                                            this.state
                                                                .isLoadingOutput
                                                                ? 'Loading...'
                                                                : child.output
                                                        }
                                                        className='form-control'
                                                    ></textarea>
                                                </div>
                                                <div className='col-2  code-nav-btn'>
                                                    <p>
                                                        {child.isSuccessSubmit
                                                            ? 'Submited!'
                                                            : ''}
                                                    </p>
                                                    <button
                                                        className='btn btn-success mb-3'
                                                        onClick={
                                                            this.handleTest
                                                        }
                                                    >
                                                        Test
                                                    </button>
                                                    <button
                                                        className='btn btn-success'
                                                        onClick={
                                                            this.handleSubmit
                                                        }
                                                    >
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </Tab>
                                    );
                                })}
                            </Tabs>
                        )}
                    </div>
                    <div className='nav-button'>
                        <button className='btn nav-btn'>
                            <i class='fa fa-microphone' aria-hidden='true'></i>
                        </button>
                        <button className='btn nav-btn'>
                            <i class='fa fa-shield' aria-hidden='true'></i>
                        </button>
                        <button className='btn nav-btn'>
                            <i class='fas fa-comment-dots'></i>
                        </button>
                        <button className='btn nav-btn'>
                            <i class='fas fa-info-circle'></i>
                        </button>
                        <button
                            class='btn leave-btn'
                            onClick={() => {
                                if (
                                    window.confirm(
                                        'Are you sure to leave the interview?'
                                    )
                                )
                                    this.handleLeaveButton();
                            }}
                        >
                            Leave
                        </button>
                    </div>
                </>
            );
        }

        return (
            <div className='room-container row'>
                <div className='col-9'>{content}</div>
                <div className='col chat-col'>
                    <div className='participants'>
                        <div className='user-info'>
                            <div>Interviewer: {this.state.roomMasterName}</div>
                            <div>Room ID: {this.state.roomLoginString}</div>
                        </div>
                    </div>
                    <div className='chat-area'>
                        <Message data={this.state.messages}></Message>
                    </div>

                    <div className='chat-input'>
                        <input
                            onChange={this.setUserMess}
                            value={this.state.userMess}
                            className='input-form'
                            id='message-text'
                            type='text'
                        ></input>
                        <button
                            onClick={this.appendMessage}
                            className='send-button'
                        >
                            <TelegramIcon></TelegramIcon>
                            Send
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export { RoomPage };
