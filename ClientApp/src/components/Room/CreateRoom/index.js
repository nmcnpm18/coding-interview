import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import './createRoom.css';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import { authHeader } from '../../../helpers';

export function CreateRoom() {
    const history = useHistory();
    const [roomName, setRoomName] = useState(false);
    const [roomPsw, setRoomPsw] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    const [isSuccess, setSuccess] = useState(false);
    const user = localStorage.getItem('user');

    if (!user) {
        history.push('/login');
    }

    const handleRoomName = (e) => {
        setRoomName(e.target.value);
    };

    const handleRoomPsw = (e) => {
        setRoomPsw(e.target.value);
    };

    const handleCreateRoom = async (e) => {
        e.preventDefault();

        const result = {
            Name: roomName,
            Password: roomPsw,
        };

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                ...authHeader(),
            },
            body: JSON.stringify(result),
        };

        await fetch(
            `${process.env.REACT_APP_API}/room/create`,
            requestOptions
        ).then((result) => {
            return result.text().then((text) => {
                const data = text && JSON.parse(text);

                console.log(data);

                if (result.ok) {
                    history.push({
                        pathname: '/room',
                        state: {
                            ...data,
                            roomName: roomName,
                            isMaster: true,
                            hasAccess: true,
                        },
                    });
                } else {
                    setErrMsg(data);
                }
            });
        });
    };

    return (
        <div className='create-room-container'>
            <form>
                <div class='form-group'>
                    <label for='room-name'>Room name</label>
                    <input
                        type='text'
                        class='form-control'
                        id='room-name'
                        onChange={handleRoomName}
                    />
                </div>
                <div class='form-group'>
                    <label for='room-psw'>Password</label>
                    <input
                        type='password'
                        class='form-control'
                        id='room-psw'
                        onChange={handleRoomPsw}
                    />
                </div>
                <div className='err-msg'>
                    <p>{errMsg ? errMsg : null}</p>
                </div>
                <button onClick={handleCreateRoom} className='btn btn-success'>
                    Create
                </button>
            </form>
        </div>
    );
}
