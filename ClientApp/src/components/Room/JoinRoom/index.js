import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

export function JoinRoom() {
    const [candidateName, setCandidateName] = useState(false);
    const [roomId, setRoomId] = useState(false);
    const [roomPassword, setRoomPassword] = useState(false);
    const [err, setError] = useState(false);
    const history = useHistory();

    const handleJoinRoom = async (e) => {
        e.preventDefault();

        const result = {
            inputName: candidateName,
            inputRoomString: roomId,
            inputRoomPassword: roomPassword,
        };

        const requestOptions = {
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            method: 'POST',
            body: JSON.stringify(result),
        };

        await fetch(
            `${process.env.REACT_APP_API}/room/joinRoom`,
            requestOptions
        ).then((response) => {
            return response.text().then((text) => {
                const data = text && JSON.parse(text);
                console.log(data);

                if (response.ok) {
                    if (data.err) {
                        return setError(data.err);
                    } else {
                        history.push({
                            pathname: '/room',
                            state: {
                                ...data,
                                isMaster: false,
                                hasAccess: true,
                                candidateName: candidateName,
                            },
                        });
                    }
                } else {
                    return setError('Error occurred!');
                }
            });
        });
    };

    const handleCandidateName = (e) => {
        setCandidateName(e.target.value);
    };

    const handleRoomId = (e) => {
        setRoomId(e.target.value);
    };

    const handleRoomPassword = (e) => {
        setRoomPassword(e.target.value);
    };

    return (
        <div className='create-room-container'>
            <form>
                <div class='form-group'>
                    <label for='room-name'>Your name</label>
                    <input
                        type='text'
                        class='form-control'
                        id='candidate-name'
                        onChange={handleCandidateName}
                    />
                </div>
                <div class='form-group'>
                    <label for='room-id'>Room ID</label>
                    <input
                        type='text'
                        class='form-control'
                        id='room-id'
                        onChange={handleRoomId}
                    />
                </div>
                <div class='form-group'>
                    <label for='room-psw'>Password</label>
                    <input
                        type='password'
                        class='form-control'
                        id='room-psw'
                        onChange={handleRoomPassword}
                    />
                </div>
                <div>
                    <p>{err ? err : null}</p>
                </div>
                <button className='btn btn-success' onClick={handleJoinRoom}>
                    Join
                </button>
            </form>
        </div>
    );
}
