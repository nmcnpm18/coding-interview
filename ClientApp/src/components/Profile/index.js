import React, { Component } from 'react';

export class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: false,
        };
    }

    componentDidMount() {
        let user = localStorage.getItem('user');

        if (user) {
            user = JSON.parse(user);
        } else {
            this.props.history.push('/login');
        }
        console.log(user);
        this.setState({ user: user });
    }

    render() {
        return (
            <div>
                <h1>Your profile</h1>
                <div className='card'>
                    <p>{this.state.user.name}</p>
                    <p>{this.state.user.email}</p>
                </div>
            </div>
        );
    }
}
