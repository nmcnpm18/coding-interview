import React, { useState } from 'react';
import { useHistory, Redirect } from 'react-router-dom';
import { Login } from '../Account/Login';

import './home.css';

function Home() {
    const history = useHistory();

    const user = localStorage.getItem('user');
    const userInfo = user && JSON.parse(user);

    const handleCreateRoom = () => {
        if (!userInfo) {
            return history.push('/login');
        } else {
            return history.push('/create-room');
        }
    };

    const handleJoinRoom = () => {
        history.push('/join-room');
    };

    return (
        <div className='container'>
            <div className='btn-container d-flex flex-column align-items-center justify-content-center'>
                <button
                    className='btn btn-success mb-3'
                    onClick={handleCreateRoom}
                >
                    Create Room
                </button>
                <button className='btn btn-success' onClick={handleJoinRoom}>
                    Join Room
                </button>
            </div>
        </div>
    );
}

export { Home };
