import React from 'react';
import { useHistory } from 'react-router-dom';
import './rating.css';
import { authHeader } from '../../helpers/auth-header';

export function Rating(props) {
    const history = useHistory();
    const handleRating = (e) => {
        const ratingValue = e.target.value;

        const state = props.location.state;

        const requestBody = {
            roomID: `${state.roomID}`,
            Rating: ratingValue,
        };

        console.log(requestBody);

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                ...authHeader(),
            },
            body: JSON.stringify(requestBody),
        };

        fetch(
            `${process.env.REACT_APP_API}/room/updaterating`,
            requestOptions
        ).then((response) => {
            return response.text().then((text) => {
                const data = text && JSON.parse(text);

                if (response.ok) {
                    console.log(data);
                } else {
                    console.log(data);
                }
            });
        });

        history.push('/');
    };
    return (
        <div>
            <h1>Please rate the candidate!</h1>
            <div className='rating'>
                <button className='rating-btn' value={1} onClick={handleRating}>
                    1
                </button>
                <button className='rating-btn' value={2} onClick={handleRating}>
                    2
                </button>
                <button className='rating-btn' value={3} onClick={handleRating}>
                    3
                </button>
                <button className='rating-btn' value={4} onClick={handleRating}>
                    4
                </button>
                <button className='rating-btn' value={5} onClick={handleRating}>
                    5
                </button>
            </div>
        </div>
    );
}
