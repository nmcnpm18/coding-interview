import { authHeader } from '../helpers';

export const userService = {
    login,
    logout,
};

const baseUrl = process.env.REACT_APP_API;

function login(email, password) {
    logout();

    const user = { Email: email, Password: password };

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user),
    };

    return fetch(`api/users/login`, requestOptions)
        .then(handleResponse)
        .then(
            (user) => {
                localStorage.setItem('user', JSON.stringify(user));
                return Promise.resolve(user);
            },
            (err) => {
                return Promise.reject(err);
            }
        );
}

function logout() {
    localStorage.removeItem('user');
}

function handleResponse(response) {
    return response.text().then((text) => {
        const data = text && JSON.parse(text);

        if (!response.ok) {
            if (response.status === 401) {
                logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
