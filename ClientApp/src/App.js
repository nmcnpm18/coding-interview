import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Login } from './components/Account/Login';
import { Register } from './components/Account/Register';
import { Home } from './components/Home';
import { NotFound } from './components/NotFound';
import { CreateRoom } from './components/Room/CreateRoom';
import { JoinRoom } from './components/Room/JoinRoom';
import { RoomPage } from './components/Room/RoomPage';
import { About } from './components/About';
import { Profile } from './components/Profile';
import { Rating } from './components/Rating';

import './styles.css';

export default class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/about' component={About} />
                    <Route exact path='/profile' component={Profile} />
                    <Route exact path='/register' component={Register} />
                    <Route exact path='/create-room' component={CreateRoom} />
                    <Route exact path='/join-room' component={JoinRoom} />
                    <Route exact path='/room' component={RoomPage} />
                    <Route exact path='/rating' component={Rating} />
                    <Route path='*' component={NotFound} />
                </Switch>
            </Layout>
        );
    }
}
