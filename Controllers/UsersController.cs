﻿using chat_app.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper; 
using System.Security.Cryptography;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using System.Security.Claims; 
using chat_app.Services;
using chat_app.Helpers;
using chat_app.Entities;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;

namespace chat_app.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;
        public UsersController(IUserService userService,
                                IMapper mapper, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }
      
        [AllowAnonymous]
        [HttpPost("register")]
        public ActionResult Register([FromBody] RegisterFormModel userRegister)
        {
            Console.WriteLine(userRegister.Email);
            var user = _mapper.Map<User>(userRegister); 
            try
            {
                _userService.Create(user, userRegister.Password);
                return Ok();
            }
            catch(ApplicationException exception)
            {
                return BadRequest(new { message = exception.Message });
            }
            
        }

        [AllowAnonymous]
        [HttpPost("login")]

        public ActionResult LogIn([FromBody] LoginFormModel loginModel)
        {
            User success = _userService.Authenticate(loginModel.Email, loginModel.Password);
           
            if(success==null)
            {
                return  BadRequest(new { message = "Email or password is incorrect" });
            }
            Console.WriteLine($"Login OK, sending token, for id {success.ID}");
            // generate tokens if login sucessfully
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            var key = new SymmetricSecurityKey( Encoding.UTF8.GetBytes(_appSettings.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512);


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("ID", success.ID.ToString()),
                    new Claim(ClaimTypes.Email, success.Email)
                }),
                SigningCredentials = credentials,
            };


            var token = tokenHandler.CreateToken(tokenDescriptor);

            var tokenString = tokenHandler.WriteToken(token);

            return Ok(new
            {
                Id = success.ID,
                Name = success.LastName + success.FirstName,
                Email = success.Email,
                Token = tokenString
            }) ;

        }

        
        [HttpGet("secret")]
        [Authorize]
        public ActionResult SecretTestPage()
        {
            var currentUser = HttpContext.User;
            if(currentUser == null) { 
                Console.WriteLine(" request don't have user field");
                return Ok(new
                {
                    userField = "no"
                }); 
            }
            else
            {
               if(  currentUser.HasClaim(e => e.Type == ClaimTypes.Email))
                {
                    return Ok(new
                    {
                        userField = currentUser.Claims.FirstOrDefault(cl => cl.Type == ClaimTypes.Email).Value
                    }) ;
                }
                else
                {
                    return Ok(new
                    {
                        userField = "not found email claim"
                    });
                }
              
            }

            
            
        }
      
    }
}
