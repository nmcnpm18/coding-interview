﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using chat_app.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using chat_app.Models;
using chat_app.Services;
using chat_app.Entities;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace chat_app.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private IRoomServices _roomServices;
        private IUserService _userService;

        public RoomController(IRoomServices roomServices, IUserService userService, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _roomServices = roomServices;
            _userService = userService;
        }


        [HttpPost("create")]
        public ActionResult CreateNewRoom([FromBody] NewRoomFormModel form)
        {
            Console.WriteLine(HttpContext.User.Identity.IsAuthenticated);
            var currentUser = HttpContext.User;
            string idField = "";

            if (currentUser == null)
            {
                Console.WriteLine(" request don't have user field");
                return Ok(new
                {
                    error = "no user field in requst"
                });
            }
            else
            {
                if (currentUser.HasClaim(c => c.Type == "ID"))
                {
                    idField = currentUser.Claims.FirstOrDefault(c1 => c1.Type == "ID").Value;
                }
                else
                {
                    return Ok(new
                    {
                        error = "Can file your id from claim"
                    });
                }
            }
            Room newRoom = _roomServices.CreateRoomInterviewer(int.Parse(idField), form.Name, form.Password);

            return Ok(newRoom.ToObject()
            );
        }

        [AllowAnonymous]
        [HttpPost("joinRoom")]
        public ActionResult JoinRoom([FromBody] JoinRoomFormModel joinRoomForm)
        {
            Room result = new Room();
            Console.WriteLine(HttpContext.User.Identity.IsAuthenticated);
            var currentUser = HttpContext.User;
            bool isSignedIn = HttpContext.User.Identity.IsAuthenticated;
            if (currentUser.HasClaim(c => c.Type == "ID"))
            {
                isSignedIn = true;
            }
            string roomLoginString = joinRoomForm.inputRoomString;
            string roomPassword = joinRoomForm.inputRoomPassword;
            string roomInputName = "";


            if (isSignedIn)
            {
                Console.WriteLine("Joining room with user: ");
                Console.WriteLine(currentUser.Claims.FirstOrDefault(c1 => c1.Type == "ID").Value);
                roomInputName = "Signed in user";
                Console.WriteLine(roomInputName);

                int parseID = int.Parse(currentUser.Claims.FirstOrDefault(c1 => c1.Type == "ID").Value);
                User user = _userService.GetById(parseID);
                if (user != null)
                {
                    string username = user.FirstName + " " + user.LastName;
                    result = _roomServices.JoinRoom(roomLoginString, roomPassword, username);
                }



            }
            else
            {
                Console.WriteLine("Joining room without user: ");
                roomInputName = joinRoomForm.inputName;
                Console.WriteLine(roomInputName);
                result = _roomServices.JoinRoom(roomLoginString, roomPassword, roomInputName);
            }

            if (result == null)
            {
                return Ok(new { err = "Fail login to room, wrong infomation or the room already have a participant" });
            }
            else
            {
                return Ok(result.ToObject());
            }


        }

        [HttpGet("loadRooms")]
        public ActionResult GetUserRooms()
        {
            var currentUser = HttpContext.User;
            if (currentUser == null)
            {
                Console.WriteLine(" request don't have user field");
                return Ok(new
                {
                    error = "no user field in requst"
                });
            }
            else
            {
                List<Room> result = new List<Room>();
                if (currentUser.HasClaim(c => c.Type == "ID"))
                {
                    string idField = currentUser.Claims.FirstOrDefault(c1 => c1.Type == "ID").Value;
                    result = _roomServices.GetAllRoomByInterviewer(int.Parse(idField));

                    if (result == null)
                    {
                        return Ok(new { error = "Not found this user" });
                    }
                    else
                    {
                        List<object> returnJson = new List<object>();
                        foreach (var room in result)
                        {
                            returnJson.Add(room.ToObject());
                        }

                        return Ok(returnJson);
                    }

                }
            }

            return Ok(new { error = "unknown" });
        }

        [HttpPost("createProblem")]

        public ActionResult CreateProblem([FromBody] AddProblemModel model)
        {
            Problem newProblem = new Problem();

            newProblem.Title = model.Title;
            newProblem.Task = model.Task;
            newProblem.Input = model.Input;
            newProblem.Source = model.Source;
            newProblem.Language = model.Lang;
            Room found = _roomServices.GetRoomById(int.Parse(model.roomID));

            if (found == null)
                return Ok(new { error = $"No room with this ID: {model.roomID}" });
            Problem returned = _roomServices.AddProblemToRoom(found, newProblem);

            return Ok(returned.ToObject());
        }

        [AllowAnonymous]
        [HttpPost("getRoomProblems")]
        public ActionResult GetRoomProblems([FromBody] GetRoomProblemModel model)
        {
            string roomID = model.roomID;
            Room found = _roomServices.GetRoomById(int.Parse(model.roomID));

            if (found == null)
            {
                return Ok(new { error = $"No room with this ID: {model.roomID}" });
            }

            List<Problem> problems = _roomServices.GetProblemsInRoom(found);

            List<dynamic> returned = new List<dynamic>();

            int i = 1;

            foreach (var problem in problems)
            {
                dynamic newObject = problem.ToObject();

                returned.Add(newObject);
            }

            return Ok(returned);
        }

        [HttpPost("updateRating")]
        public ActionResult UpdateRoomRating([FromBody] UpdateRateModel model)
        {
            int newRating = model.Rating;
            Room found = _roomServices.GetRoomById(int.Parse(model.roomID));

            if (found == null)
            {
                return Ok(new { error = $"No room with this ID: {model.roomID}" });
            }
            Room returned = _roomServices.updateRoomRating(found, newRating);

            return Ok(returned.ToObject());
        }

        [AllowAnonymous]
        [HttpPost("updateProblem")]

        public ActionResult UpdateProblem([FromBody] UpdateProblemModel model)
        {
            // get problem 
            Problem changeto = new Problem { Input = model.Input, Language = model.Lang, Source = model.Source, Output = model.Output };
            int id = int.Parse(model.ID);
            // call service to change
            Problem result = _roomServices.updateProblem(id, changeto);
            return Ok(result.ToObject());
        }
    }
}
