﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using chat_app.Entities;
using chat_app.Helpers;
using System.Security;

namespace chat_app.Services
{


    public interface IUserService
    {
        User Authenticate(string email, string password);

        User GetById(int id);
        User Create(User user, string password);
        void Delete(int id);
    }
    public class UserService : IUserService
    {
        private SqlServerDataContext _dataContext;

        public UserService(SqlServerDataContext dataContext)
        {
            _dataContext = dataContext; 
        }
        public User Authenticate(string email, string password)
        {
            User found = _dataContext.Users.SingleOrDefault(u => u.Email == email);
            if(found != null)
            {
                Console.WriteLine($"Found user: {found.Email}");
            }
            else
            {
                Console.WriteLine($"Not found user");
                return null;
            }
            
            if (!CheckUserLogin(password, found.PasswordHash, found.PasswordSalt))
            {
                return null;
            }

            return found; 
        }

        public User Create(User user, string password)
        {
            if(string.IsNullOrWhiteSpace(password))
            {
                throw new ApplicationException("Password required"); 
            }
            if(_dataContext.Users.Any(
                x => x.Email == user.Email ))
            {
                throw new ApplicationException($"Email: {user.Email} already takens");
            }

            HashPassword(password, out var newPasswordSalt, out var newPasswordHash);
            user.PasswordHash = newPasswordHash;
            user.PasswordSalt = newPasswordSalt;

            _dataContext.Add(user);
            _dataContext.SaveChanges();

            return user;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
        public User GetById(int id)
        {
            User found = _dataContext.Users.SingleOrDefault(u => u.ID == id);
            if (found != null)
            {
                Console.WriteLine($"Found user: {found.Email}");
            }
            else
            {
                Console.WriteLine($"Not found user");
                return null;
            }
            return found;
        }


        private void HashPassword(string inputPassword, out byte[] salt, out byte[] hash)
        {
            if(inputPassword == null || string.IsNullOrWhiteSpace(inputPassword))
            {
                throw new ArgumentException("Passoword is undefied"); 
            }

            using (var randomhash = new System.Security.Cryptography.HMACSHA512())
            {
                salt = randomhash.Key;
                hash = randomhash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(inputPassword)); 

            }
        }

        private static bool CheckUserLogin(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null)
                throw new ArgumentException("Input password is empty");
            using(var x = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var passwordHashToCheck = x.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)); 
                for(int i =0; i < passwordHashToCheck.Length; i++)
                {
                    if(passwordHashToCheck[i] != storedHash[i])
                    {
                        Console.WriteLine("Wrong password");
                        return false;
                    }
                }
            }
            
            return true; 
        }

        
    }
}
