﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using chat_app.Entities;
using chat_app.Helpers;
using Microsoft.EntityFrameworkCore;


namespace chat_app.Services
{
    public interface IRoomServices
    {

        Room GetRoomById(int roomId);
      
        Room CreateRoomInterviewer(int userID, string roomName, string roomPassword);

        List<Room> GetAllRoomByInterviewer(int userID);

        Room JoinRoom(string inputRoomString, string inputRoomPassword, string name);

        Problem AddProblemToRoom(Room room, Problem problem);

        List<Problem> GetProblemsInRoom(Room room);

        Room updateRoomRating(Room room, int newRating);
        Problem updateProblem(int problemID, Problem changedProblem);
        
        
    }
    public class RoomServices : IRoomServices
    {
        private SqlServerDataContext _dataContext;

        private UserService userService;

        public RoomServices(SqlServerDataContext dataContext)
        {
            _dataContext = dataContext;
            userService = new UserService(_dataContext); 
        }

        public Problem AddProblemToRoom(Room room, Problem problem)
        {
            problem.Room = room; 

            _dataContext.Add(problem);
            _dataContext.SaveChanges(); 

            return problem; 
        }

        public Room CreateRoomInterviewer(int userID, string roomName, string roomPassword)
        {
            Room result = new Room();
            result.Name = roomName;
            result.Password = roomPassword;
            result.CurrentRating = 0;
            result.IntervieweeName = "";

            User masterUser = userService.GetById(userID); 
            if(masterUser == null)
            {
                Console.WriteLine("Fail to get master user while creating room");
                return null; 
            }
            result.MasterUser = masterUser;


            result.LoginString = RandomString(10); 

           

            _dataContext.Add(result); 
            _dataContext.SaveChanges();


            Console.WriteLine($"Newly created room: id : {result.ID}"); 

            return result; 
        }

        

        public List<Room> GetAllRoomByInterviewer(int userID)
        {
            User masterUser = userService.GetById(userID);
            if (masterUser == null)
            {
                Console.WriteLine("Fail to get master user while creating room");
                return null;
            }
            List<Room> result = new List<Room>();
            result = _dataContext.Rooms.Where(ro => ro.MasterUser == masterUser).ToList();

            return result; 
        }

        public List<Problem> GetProblemsInRoom(Room room)
        {
            var result = _dataContext.Problems.Where(r => r.Room == room).ToList();

            return result;
        }

        public Room GetRoomById(int roomId)
        {
            Room result = _dataContext.Rooms.SingleOrDefault(r => r.ID == roomId); 

            if(result != null)
            {
                return result;
            }
            else
            {
                Console.WriteLine($"No room with ID: {roomId}");
                return null; 
            }
        }

        public Room JoinRoom(string inputRoomString, string inputRoomPassword, string name)
        {
            Room found = _dataContext.Rooms.Include(r => r.MasterUser).SingleOrDefault(r => r.LoginString == inputRoomString && r.Password == inputRoomPassword); 
            if(found == null)
            {
                return null;
            }
            else
            {
                if(found.IntervieweeName != "")
                {
                    return null;
                }
                found.IntervieweeName = name;
                _dataContext.SaveChanges();
                return found; 
            }
        }

        public string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public Problem updateProblem(int problemID, Problem changedProblem)
        {
            Problem found = _dataContext.Problems.Include(p => p.Room).SingleOrDefault(p => p.ID == problemID); 

            if(found == null)
            {
                return null;
            }
            found.Input = changedProblem.Input;
            found.Language = changedProblem.Language;
            found.Output = changedProblem.Output;
            found.Source = changedProblem.Source;

            _dataContext.SaveChanges();
            return found; 
        }

        public Room updateRoomRating(Room room, int newRating)
        {
            Room tempRoom = _dataContext.Rooms.Include(r => r.MasterUser).SingleOrDefault(r => r.ID == room.ID);

            tempRoom.CurrentRating = newRating;

            _dataContext.SaveChanges();

            return tempRoom;
        }
    }
}
