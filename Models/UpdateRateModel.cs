﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace chat_app.Models
{
    public class UpdateRateModel
    {
        [Required]
        public string roomID { get; set; }
        [Required]
        public int Rating { get; set; }
    }
}
