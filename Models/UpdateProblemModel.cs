﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chat_app.Models
{
    public class UpdateProblemModel
    {
        public string ID { get; set; }

        public string Input { get; set; }

        public string Output { get; set; }

        public string Source { get; set; }

        public string Lang { get; set; }
    }
}
