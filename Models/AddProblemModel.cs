﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace chat_app.Models
{
    public class AddProblemModel
    {
        [Required]
        public string roomID { get; set; }
        [Required]
        public string Title { get; set;  }
        [Required]
        public string Task { get; set; }
        
        public string Input { get; set; }

        public string Source { get; set; }

        public string Lang {get;set;}
    }
}
