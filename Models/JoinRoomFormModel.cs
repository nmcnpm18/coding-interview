﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace chat_app.Models
{
    public class JoinRoomFormModel
    {
        public string inputName { get; set;}
        [Required]
        public string inputRoomString { get; set; }
        [Required] 
        public string inputRoomPassword { get; set; }
    }
}
