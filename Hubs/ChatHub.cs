﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Protocol;

namespace chat_app.Hubs
{
    public class ChatHub: Hub
    {
        public Task JoinRoom(int roomID)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, roomID.ToString());
        }

        public Task SendMessegeToRoom(int roomID, string user, string message)
        {
            return Clients.Group(roomID.ToString()).SendAsync("ReceiveMessage", user, message);
        }
    }
}
